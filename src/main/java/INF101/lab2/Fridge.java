package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	int maximum;
	ArrayList<FridgeItem> FridgeItems;
	
	public Fridge() {
		FridgeItems = new ArrayList<>();
		maximum = 20;
	}
	
	@Override
	public int nItemsInFridge() {
		return FridgeItems.size();
	}

	@Override
	public int totalSize() {
		return maximum;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if(item == null) {
			throw new IllegalArgumentException("item with value null");
		}
		if (FridgeItems.size() >= maximum) {
			return false;
		}
		return FridgeItems.add(item);
		// return true; add() returns true value so we dont need this
	}

	@Override
	public void takeOut(FridgeItem item) {
		if(item == null) {
			throw new IllegalArgumentException("item with value null");
		}
		if (!FridgeItems.contains(item)) {
			throw new NoSuchElementException("item not in fridge");
		}
		FridgeItems.remove(item);
	}

	@Override
	public void emptyFridge() {
		FridgeItems.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> GoodFood = new ArrayList<>();
		for (FridgeItem item: FridgeItems) {
			if (item.hasExpired()) {
				continue;
			} else {
				GoodFood.add(item);
			}
		}
		FridgeItems.removeAll(GoodFood);
		return FridgeItems;
	} 		
}
